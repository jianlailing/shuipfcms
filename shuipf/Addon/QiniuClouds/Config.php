<?php

/**
 * 插件配置，下面是示例
 * Some rights reserved：abc3210.com
 * Contact email:admin@abc3210.com
 */
return array(
    'qiniubucket' => array(
        'title' => '空间名称:', //表单的文字
        'type' => 'text', //表单的类型：text、textarea、checkbox、radio、select等
        'value' => '', //表单的默认值
        'style' => "width:200px;", //表单样式
    ),
    'qiniuuppat' => array(
        'title' => '上传路径前缀:', //表单的文字
        'type' => 'text', //表单的类型：text、textarea、checkbox、radio、select等
        'value' => '/', //表单的默认值
        'tips' => '如果这里设置非"/"，网站配置中的，附件访问地址也要设置相应目录。',
        'style' => "width:200px;", //表单样式
    ),
    'qiniuak' => array(
        'title' => '七牛AK:', //表单的文字
        'type' => 'text', //表单的类型：text、textarea、checkbox、radio、select等
        'value' => '', //表单的默认值
        'style' => "width:300px;", //表单样式
    ),
    'qiniusk' => array(
        'title' => '七牛SK:', //表单的文字
        'type' => 'text', //表单的类型：text、textarea、checkbox、radio、select等
        'value' => '', //表单的默认值
        'style' => "width:300px;", //表单样式
    ),
);
