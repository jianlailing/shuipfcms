<?php

/**
 * 七牛云存储 插件
 * Some rights reserved：abc3210.com
 * Contact email:admin@abc3210.com
 */

namespace Addon\QiniuClouds;

use \Addons\Util\Addon;

class QiniuCloudsAddon extends Addon {

    //插件信息
    public $info = array(
        'name' => 'QiniuClouds',
        'title' => '七牛云存储',
        'description' => '七牛云存储',
        'status' => 1,
        'author' => '水平凡',
        'version' => '1.0.0',
        'has_adminlist' => 0,
        'sign' => '6fcdf42a3f827dd35c66957a1b8d0a57',
    );

    //安装
    public function install() {
        if (version_compare(SHUIPF_VERSION, '2.0.3', '<')) {
            $this->error = '本插件需要 ShuipFCMS 2.0.3 版本才支持！';
            return false;
        }
        $dirPath = PROJECT_PATH . 'Libs/Driver/Attachment/';
        if (!is_writable($dirPath)) {
            $this->error = "目录[{$dirPath}]不可写！";
            return false;
        }
        if (file_exists($dirPath . 'Qiniu.class.php')) {
            $this->error = "文件[{$dirPath}Qiniu.class.php]已经存在！";
            return false;
        }
        //复制驱动
        $dir = new \Dir();
        if ($dir->copyDir($this->addonPath . 'Attachment/', $dirPath) === false) {
            $this->error = '移动文件失败！';
            return false;
        }
        D('Config')->where(array('varname' => 'attachment_driver'))->save(array('value' => 'Qiniu'));
        cache('Config', NULL);
        return true;
    }

    //卸载
    public function uninstall() {
        $dirPath = PROJECT_PATH . 'Libs/Driver/Attachment/';
        unlink($dirPath . 'Qiniu.class.php');
        D('Config')->where(array('varname' => 'attachment_driver'))->save(array('value' => 'Local'));
        cache('Config', NULL);
        return true;
    }

}
