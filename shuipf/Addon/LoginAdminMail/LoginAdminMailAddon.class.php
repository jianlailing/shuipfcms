<?php

/**
 * 后台登录成功邮件通知 插件
 * Some rights reserved：abc3210.com
 * Contact email:admin@abc3210.com
 */

namespace Addon\LoginAdminMail;

use \Addons\Util\Addon;

class LoginAdminMailAddon extends Addon {

    //插件信息
    public $info = array(
        'name' => 'LoginAdminMail',
        'title' => '后台登录成功邮件通知',
        'description' => '后台登录成功邮件通知',
        'status' => 1,
        'author' => '水平凡',
        'version' => '1.0.0',
        'has_adminlist' => 0,
        'sign' => '08915e61ac5cc9119255b8fd13ab8e01',
    );

    //后台登录成功，信息提示
    public function admin_public_tologin($data) {
        $config = $this->getAddonConfig();
        if (empty($config['email'])) {
            return false;
        }
        if (empty($config['content'])) {
            $config['content'] = '网站管理员您好，刚刚有用户 {username} 登录了网站后台，登录者IP：{ip}，请注意是否正常。';
        }
        $config['content'] = str_replace(array(
            '{username}',
            '{ip}',
            '{date}',
                ), array(
            $data['username'],
            $data['ip'],
            date('Y-m-d H:i:s'),
                ), $config['content']);
        SendMail($config['email'], '网站后台登录提醒', $config['content']);
        return true;
    }

    //安装
    public function install() {
        $Behavior = D('Common/Behavior');
        C('TOKEN_ON', false);
        if ($Behavior->where(array('name' => 'admin_public_tologin'))->count() == 0) {
            $Behavior->addBehavior(array(
                'name' => 'admin_public_tologin',
                'title' => '网站后台登录成功行为调用',
                'type' => 1,
            ));
        }
        return true;
    }

    //卸载
    public function uninstall() {
        return true;
    }

}
