<?php

/**
 * 插件配置，下面是示例
 * Some rights reserved：abc3210.com
 * Contact email:admin@abc3210.com
 */
return array(
    'email' => array(//配置在表单中的键名 ,这个会是config[title]
        'title' => '邮箱地址:', //表单的文字
        'type' => 'text', //表单的类型：text、textarea、checkbox、radio、select等
        'value' => '', //表单的默认值
        'style' => "width:200px;", //表单样式
    ),
    'content' => array(
        'title' => '邮件内容:',
        'type' => 'textarea',
        'value' => '网站管理员您好，刚刚有用户 {username} 登录了网站后台，登录者IP：{ip}，请注意是否正常。',
        'style' => "width:400px;",
        'tips' => '{username}表示登录的用户名，{ip}表示登录者IP,{date}时间',
    ),
);
