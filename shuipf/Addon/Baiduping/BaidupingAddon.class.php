<?php
/**
 * 百度ping 插件
 * Some rights reserved：abc3210.com
 * Contact email:admin@abc3210.com
 */

namespace Addon\Baiduping;

use \Addons\Util\Addon;

class BaidupingAddon extends Addon
{

    //插件信息
    public $info = array(
        'name' => 'Baiduping',
        'title' => '百度ping',
        'description' => '百度ping<br><span style="color:red;font-weight:bolder;">每天50条 超过了就不起作用了</span>',
        'status' => 1,
        'author' => '随风',
        'version' => '1.0',
        'has_adminlist' => 0,
        'sign' => '6513F54199649B9AF86A6648E579A214',
    );

    //安装
    public function install()
    {
        //检查行为是否有添加
        $Behavior = D('Common/Behavior');
        C('TOKEN_ON', false);
        if ($Behavior->where(array('name' => 'content_add_end'))->count() == 0) {
            $Behavior->addBehavior(array(
                'name' => 'content_add_end',
                'title' => '内容添加前行为结束调用',
                'type' => 1,
            ));
        }
        return true;
    }

    //卸载
    public function uninstall()
    {
        return true;
    }

    public function content_add_end(&$data)
    {

        return $this->Baiduping($data);
    }

    protected function Baiduping(&$data)
    {
        if (empty($data)) {
            return false;
        }
        //插件配置
        $config = $this->getAddonConfig();
        if (!$config['kaiguan'] || empty($config['site']) || empty($config['token'])) {
            return false;
        }
        $api = "http://data.zz.baidu.com/urls?site={$config['site']}&token={$config['token']}";
        $ch = curl_init();
        $options = array(
            CURLOPT_URL => $api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => $data[url],
            CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
        );
        curl_setopt_array($ch, $options);
        $result = curl_exec($ch);
        $obj = json_decode($result, true);
        return $obj[remain];
    }
}
