<?php

/**
 * 插件配置，下面是示例
 * Some rights reserved：abc3210.com
 * Contact email:admin@abc3210.com
 */
return array(
  'kaiguan' => array(
        'title' => '是否开启本插件:',
        'type' => 'radio',
        'options' => array(
            '1' => '开启',
            '0' => '关闭'
        ),
        'value' => '1'
    ),
    'site' => array(
        'title' => '站点url:', 
        'type' => 'text',
        'value' => '',
        'style' => "width:400px;",
		 'tips' => '在站长平台验证的站点，比如www.example.com',
    ),
	 'token' => array(
        'title' => '密钥:', 
        'type' => 'text',
        'value' => '',
        'style' => "width:400px;",
		 'tips' => '在站长平台申请的推送用的准入密钥',
    ),
   
  
  
    
   
);
