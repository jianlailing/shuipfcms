<?php

// +----------------------------------------------------------------------
// | ShuipFCMS 插件前台管理
// +----------------------------------------------------------------------
// | Copyright (c) 2012-2014 http://www.shuipfcms.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: 水平凡 <admin@abc3210.com>
// +----------------------------------------------------------------------

namespace Addon\Report\Controller;

use Addons\Util\AddonsBase;

class IndexController extends AddonsBase {

    //自动验证
    protected $_validate = array(
        //array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间)
        array('info_id', 'require', '信息ID不能为空！', 1, 'regex', 3),
        array('catid', 'require', '栏目ID不能为空！', 1, 'regex', 3),
        array('reason', 'require', '举报原因不能为空！', 1, 'regex', 3),
    );
    //自动完成
    protected $_auto = array(
        //array(填充字段,填充内容,填充条件,附加规则)
        array('ip', 'get_client_ip', 1, 'function'),
        array('report_time', 'time', 1, 'function'),
        array('status', 0),
    );

    public function index() {
        if (IS_POST) {
            $validate = I('post.validate');
            if (empty($validate)) {
                $this->error('请输入验证码！');
            }
            if ($this->verify($validate, 'report') == false) {
                $this->error('验证码错误，请重新输入！');
            }
            if (cookie('report')) {
                $this->error('您刚举报过信息不久，喝杯咖啡歇歇~');
            }
            if (M('Report')->auto($this->_auto)->validate($this->_validate)->create()) {
                M('Report')->title = \Content\Model\ContentModel::getInstance(getCategory(M('Report')->catid, 'modelid'))->where(array('id' => M('Report')->info_id, 'status' => 99))->getField('title');
                M('Report')->userid = service("Passport")->userid? : 0;
                M('Report')->username = service("Passport")->username? : '游客';
                if (M('Report')->add()) {
                    $config = $this->getAddonConfig();
                    if ($config['fen']) {
                        cookie('report', 1, $config['fen']);
                    }
                    $this->success('举报成功，我们将尽快核实处理！', cache('Config.siteurl'));
                } else {
                    $this->error(M('Report')->getError()? : '举报失败！');
                }
            } else {
                $this->error(M('Report')->getError()? : '举报失败！');
            }
        } else {
            $catid = I('get.catid', 0, 'intval');
            $id = I('get.id', 0, 'intval');
            if (empty($catid) || empty($id)) {
                $this->error('请指定需要举报的信息！');
            }
            $info = \Content\Model\ContentModel::getInstance(getCategory($catid, 'modelid'))->where(array('id' => $id, 'status' => 99))->getField('id,title,url');
            if (empty($info)) {
                $this->error('该信息不存在！');
            }
            $this->assign('info_id', $id);
            $this->assign('info', $info[$id]);
            $this->assign('catid', $catid);
            $this->display();
        }
    }

}
