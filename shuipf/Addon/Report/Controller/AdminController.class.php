<?php

// +----------------------------------------------------------------------
// | ShuipFCMS 插件后台管理
// +----------------------------------------------------------------------
// | Copyright (c) 2012-2014 http://www.shuipfcms.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: 水平凡 <admin@abc3210.com>
// +----------------------------------------------------------------------

namespace Addon\Report\Controller;

use Addons\Util\Adminaddonbase;

class AdminController extends Adminaddonbase {

    public function index() {
        if (IS_POST) {
            $_POST['isadmin'] = 1;
            $this->redirect('index', $_POST);
        }
        $where = array();
        $status = I('get.status', 0, 'intval');
        if ($_GET['status'] != '') {
            $where['status'] = $status;
            $this->assign('status', $status);
        }
        $keyword = I('get.keyword', '', 'trim');
        if (!empty($keyword)) {
            $where['title'] = array('LIKE', "%{$keyword}%");
            $this->assign('keyword', $keyword);
        }
        $this->basePage(M('Report'), $where, array('id' => 'DESC'), 20);
    }

    public function handle() {
        $id = I('get.id', 0, 'intval');
        $info = M('Report')->where(array('id' => $id))->find();
        if (empty($info)) {
            $this->error('该举报信息不存在！');
        }
        if (M('Report')->where(array('id' => $id))->save(array('status' => 1)) !== false) {
            $config = $this->getAddonConfig();
            if ($config['handle']) {
                \Content\Model\ContentModel::getInstance(getCategory($info['catid'], 'modelid'))->where(array('id' => $info['info_id']))->save(array('status' => 1));
            }
            $this->success('操作成功！');
        } else {
            $this->error('操作失败！');
        }
    }

    public function falses() {
        $id = I('get.id', 0, 'intval');
        $info = M('Report')->where(array('id' => $id))->find();
        if (empty($info)) {
            $this->error('该举报信息不存在！');
        }
        if (M('Report')->where(array('id' => $id))->save(array('status' => 2)) !== false) {
            $this->success('操作成功！');
        } else {
            $this->error('操作失败！');
        }
    }

    public function delete() {
        $this->baseDelete(M('Report'), 'index?isadmin=1');
    }

}
