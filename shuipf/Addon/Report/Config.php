<?php

return array(
    'fen' => array(//配置在表单中的键名 ,这个会是config[title]
        'title' => '举报间隔时间:', //表单的文字
        'type' => 'text', //表单的类型：text、textarea、checkbox、radio、select等
        'value' => '60', //表单的默认值
        'style' => "width:200px;", //表单样式
        'tips' => '单位秒，0为不限制',
    ),
    'handle' => array(
        'title' => '已处理:',
        'type' => 'select',
        'options' => array(
            '0' => '不进行操作',
            '1' => '被举报信息修改为待审核状态',
        ),
        'value' => '1'
    ),
);
