<?php if (!defined('SHUIPF_VERSION')) exit(); ?>
<admintemplate file="Common/Head"/>
<body class="J_scroll_fixed">
<div class="wrap">
  <Admintemplate file="Common/Nav"/>
  <div class="h_a">搜索</div>
  <form method="post" action="{:U('index?isadmin=1')}">
    <div class="search_type cc mb10">
      <div class="mb10"> <span class="mr20">
        <select class="select_2" name="status"style="width:70px;">
          <option value='' <if condition="$status eq '' ">selected</if>>全部</option>
          <option value="0" <if condition="$status eq 0 && $_GET['status'] neq ''  ">selected</if>>待处理</option>
          <option value="1" <if condition="$status eq 1 ">selected</if>>已处理</option>
          <option value="2" <if condition="$status eq 2 ">selected</if>>误报</option>
        </select>
        关键字：
        <input type="text" class="input length_2" name="keyword" style="width:200px;" value="{$keyword}" placeholder="请输入关键字...">
        <button class="btn">搜索</button>
        </span> </div>
    </div>
  </form>
  <div class="table_list">
      <table width="100%" cellspacing="0">
        <thead>
          <tr>
            <td align="center" width="30">ID</td>
            <td>标题</td>
            <td>举报原因</td>
            <td align="center" width="60">状态</td>
            <td align="center" width="120">操作</td>
          </tr>
        </thead>
        <tbody>
          <volist name="data" id="vo">
            <tr>
              <td align="center">{$vo.id}</td>
              <td>{$vo.title}</td>
              <td>{$vo.reason}</td>
              <td align="center"><if condition="$vo['status'] eq 0 ">待处理<elseif condition="$vo['status'] eq 1"/>已处理<elseif condition="$vo['status'] eq 2"/>误报</if></td>
              <td align="center"> <a href="{:U('handle?isadmin=1',array('id'=>$vo['id']))}">已处理</a> | <a href="{:U('falses?isadmin=1',array('id'=>$vo['id']))}">误报</a> | <a href="{:U('delete?isadmin=1',array('id'=>$vo['id']))}" class="J_ajax_del">删除</a></td>
            </tr>
          </volist>
        </tbody>
      </table>
      <div class="p10">
        <div class="pages"> {$Page} </div>
      </div>
    </div>
</div>
<script src="{$config_siteurl}statics/js/common.js"></script>
</body>
</html>