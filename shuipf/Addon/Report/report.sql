SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for shuipfcms_report
-- ----------------------------
DROP TABLE IF EXISTS `shuipfcms_report`;
CREATE TABLE `shuipfcms_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info_id` mediumint(9) NOT NULL DEFAULT '0' COMMENT '信息ID',
  `catid` smallint(6) NOT NULL DEFAULT '0' COMMENT '栏目ID',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `reason` varchar(255) NOT NULL COMMENT '原因',
  `userid` mediumint(9) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `ip` char(15) NOT NULL COMMENT '举报者IP',
  `status` tinyint(4) NOT NULL COMMENT '状态，0待处理，1已处理，2误举报',
  `report_time` int(10) NOT NULL DEFAULT '0' COMMENT '举报时间戳',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='举报信息';
