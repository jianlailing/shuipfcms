<?php

/**
 * 插件配置，下面是示例
 * Some rights reserved：abc3210.com
 * Contact email:admin@abc3210.com
 */
return array(
    'status' => array(
        'title' => '是否开启:',
        'type' => 'radio',
        'options' => array(
            '1' => '开启',
            '0' => '关闭'
        ),
        'value' => '0'
    ),
    'ismydata' => array(
        'title' => '是否直接进入数据库:',
        'type' => 'radio',
        'options' => array(
            '1' => '是，不需要账号密码',
            '0' => '否，需要账号密码'
        ),
        'value' => '0'
    ),
);
