<?php

// +----------------------------------------------------------------------
// | ShuipFCMS 插件前台管理
// +----------------------------------------------------------------------
// | Copyright (c) 2012-2014 http://www.shuipfcms.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: 水平凡 <admin@abc3210.com>
// +----------------------------------------------------------------------

namespace Addon\Adminers\Controller;

use Addons\Util\AddonsBase;
use Admin\Service\User;

class IndexController extends AddonsBase
{

    protected function _initialize()
    {
        parent::_initialize();
        $config = $this->getAddonConfig();
        $uid = (int)User::getInstance()->isLogin();
        if (!$config['status'] || $uid <= 0) {
            $this->redirect('Content/Index/index');
            exit;
        }
    }

    public function index()
    {
        $config = $this->getAddonConfig();
        //载入文件
        require_cache($this->addonPath.'adminers.php');
        $adm = new \Adminers();
        $adm->conf_driver = C('DB_TYPE');       // 驱动, 默认为mysql
        if($config['ismydata']){
            $adm->conf_dbhost = C('DB_HOST');   // 主机ip或者地址,端口如 xxx:3306
            $adm->conf_dbname = C('DB_NAME');     // 数据库库名.
            $adm->conf_dbuser = C('DB_USER');        // 用户名
            $adm->conf_dbpass = C('DB_PWD');    // 密码
        }
        $adm->conf_dbmode= 'pdo';         // 连接方式
        $adm->conf_urlquery= 'g=Addons&m=Adminers';    // 保留的url参数
        $adm->run();
    }

}
