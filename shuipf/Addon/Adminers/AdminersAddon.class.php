<?php
/**
 * Adminers数据库管理工具 插件
 * Some rights reserved：abc3210.com
 * Contact email:admin@abc3210.com
 */

namespace Addon\Adminers;

use \Addons\Util\Addon;

class AdminersAddon extends Addon {

    //插件信息
    public $info = array(
        'name' => 'Adminers',
        'title' => 'Adminers数据库管理工具',
        'description' => '轻量级数据库管理工具，类似phpmyadmin工具',
        'status' => 1,
        'author' => 'Adminers、水平凡',
        'version' => '1.0.0',
        'has_adminlist' => 0,
        'sign' => '2fdd8aa8c9e84ed089de8ea97b1d0c77',
    );

    //安装
    public function install() {
        return true;
    }

    //卸载
    public function uninstall() {
        return true;
    }

}
