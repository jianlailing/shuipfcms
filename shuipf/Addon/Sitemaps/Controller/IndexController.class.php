<?php

// +----------------------------------------------------------------------
// | ShuipFCMS 插件前台管理
// +----------------------------------------------------------------------
// | Copyright (c) 2012-2014 http://www.shuipfcms.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: 水平凡 <admin@abc3210.com>
// +----------------------------------------------------------------------

namespace Addon\Sitemaps\Controller;

use Addons\Util\AddonsBase;

class IndexController extends AddonsBase {

    public function index() {
        $config = $this->getAddonConfig();
        if (empty($config)) {
            $config = array(
                'content_priority' => '0.7',
                'content_changefreq' => 'weekly',
                'num' => 20,
                'mark' => 1,
                'time' => 40,
                'baidunum' => 20,
                'catids' => array(),
            );
        }
        $Sitemaps = new \Addon\Sitemaps\Sitemaps();
        $Sitemaps->siteMap($config);
        $Sitemaps->baidu($config);
    }

}
