<?php

// +----------------------------------------------------------------------
// | ShuipFCMS 插件后台管理
// +----------------------------------------------------------------------
// | Copyright (c) 2012-2014 http://www.shuipfcms.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: 水平凡 <admin@abc3210.com>
// +----------------------------------------------------------------------

namespace Addon\Sitemaps\Controller;

use Addons\Util\Adminaddonbase;

class AdminController extends Adminaddonbase {

    public function index() {
        $config = $this->getAddonConfig();
        if (empty($config)) {
            $config = array(
                'content_priority' => '0.7',
                'content_changefreq' => 'weekly',
                'num' => 20,
                'mark' => 1,
                'time' => 40,
                'baidunum' => 20,
                'catids' => array(),
            );
        }
        if (IS_POST) {
            $config = $_POST;
            D('Addons/Addons')->where(array('sign' => '4202a4da1e70a5ce0bc2a44edce74e76'))->save(array('config' => serialize($config)));
            D('Addons/Addons')->addons_cache();
            $Sitemaps = new \Addon\Sitemaps\Sitemaps();
            $Sitemaps->siteMap($config);
            $Sitemaps->baidu($config);
            $this->success('生成成功！');
        } else {
            //栏目列表 可以用缓存的方式
            $array = cache('Category');
            foreach ($array as $k => $v) {
                $array[$k] = getCategory($v['catid']);
                $array[$k]['disabled'] = "";
                if (in_array($v['catid'], $config['catids'])) {
                    $array[$k]['selected'] = "selected";
                }
            }
            $this->Tree = new \Tree();
            if (!empty($array) && is_array($array)) {
                $this->Tree->icon = array('&nbsp;&nbsp;&nbsp;│ ', '&nbsp;&nbsp;&nbsp;├─ ', '&nbsp;&nbsp;&nbsp;└─ ');
                $this->Tree->nbsp = '&nbsp;&nbsp;&nbsp;';
                $str = "<option value='\$catid' \$selected \$disabled>\$spacer \$catname</option>";
                $this->Tree->init($array);
                $categorydata = $this->Tree->get_tree(0, $str, 0);
            } else {
                $categorydata = '';
            }
            $this->assign('config', $config);
            $this->assign("category", $categorydata);
            $this->display();
        }
    }

}
