<?php if (!defined('SHUIPF_VERSION')) exit(); ?>
<admintemplate file="Common/Head"/>
<body class="J_scroll_fixed">
<div class="wrap">
  <Admintemplate file="Common/Nav"/>
  <div class="h_a">Sitemaps:</div>
  <div class="prompt_text">
    <p>Sitemaps 服务旨在使用 Feed 文件 sitemap.xml 通知 Google、Yahoo! 以及 Microsoft 等 Crawler(爬虫)网站上哪些文件需要索引、这些文件的最后修订时间、更改频度、文件位置、相对优先索引权，这些信息将帮助他们建立索引范围和索引的行为习惯。详细信息请查看 sitemaps.org 网站上的说明。
      通过Sitemaps，您可以获得： </p>
    <p>1、更大的抓取范围，更新的搜索结果 – 帮助网友找到更多您的网页。</p>
    <p>2、更为智能的抓取 – 因为我们可以得知您网页的最新修改时间或网页的更改频率。</p>
    <p>3、详细的报告 – 详细说明 Google 如何将网友的点击指向您的网站及 Googlebot 如何看到您的网页。</p>
  </div>
  <div class="h_a">互联网新闻开放协议:</div>
  <div class="prompt_text">
    <p>《互联网新闻开放协议》是百度新闻搜索制定的搜索引擎新闻源收录标准，网站可将发布的新闻内容制作成遵循此开放协议的XML格式的网页（独立于原有的新闻发布形式）供搜索引擎索引。 ShuipFCMS可自动生成网站的Sitemaps，但是您还需要向google或者baidu提交Sitemaps的访问地址。</p>
    <p>您的网站的Sitemaps 访问地址为：{:cache('Config.siteurl')}sitemaps.xml</p>
    <p>ShuipFCMS可自动生成网站的<
      <互联网新闻开放协议>
      >，但是您还需要向baidu提交访问地址。</p>
    <p>您的网站的Sitemaps 访问地址为：{:cache('Config.siteurl')}baidunews.xml</p>
    <p>更多关于Google Sitemaps的信息：https://www.google.com/webmasters/sitemaps/login?hl=zh_CN</p>
    <p>更多关于<
      <互联网新闻开放协议>
      >的信息：http://news.baidu.com/newsop.html#kg</p>
  </div>
  <div class="h_a">自动生成:</div>
  <div class="prompt_text">
   <p>自动生成需要计划任务支持，可以在服务器中使用计划任务定点访问 {:U('Addons/Sitemaps/index')} 地址进行生成。</p>
  </div>
  <form method='post'   id="myform" class="J_ajaxForm"  action="{:U('index?isadmin=1')}">
    <div class="h_a">生成 Sitemaps</div>
    <div class="table_full">
      <table cellpadding=0 cellspacing=0 width="100%" class="table_form" >
        <tr>
          <th width="140">更新频率 :</th>
          <td><?php echo \Form::select(array('0.1'=>0.1,'0.2'=>0.2,'0.3'=>0.3,'0.4'=>0.4,'0.5'=>0.5,'0.6'=>0.6,'0.7'=>0.7,'0.8'=>0.8,'0.9'=>0.9,'1'=>1),$config['content_priority'],'name="content_priority"'); ?>
		  <?php echo \Form::select(array('always'=>'一直更新','hourly'=>'小时','daily'=>'天','weekly'=>'周','monthly'=>'月','yearly'=>'年','never'=>'从不更新'),$config['content_changefreq'],'name="content_changefreq"'); ?></td>
        </tr>
        <tr>
          <th>生成数量 :</th>
          <td><input type="text" name="num" value="{$config.num}" size="5" class="input"></td>
        </tr>
      </table>
    </div>
    <div class="h_a">生成 互联网新闻开放协议</div>
    <div class="table_full">
      <table cellpadding=0 cellspacing=0 width="100%" class="table_form" >
        <tr>
          <th width="140">生成XML文件 :  :</th>
          <td><?php echo \Form::radio(array(1=>'是',0=>'否',),$config['mark'],'name="mark"'); ?></td>
        </tr>
        <tr>
          <th>选择数据来源  :</th>
          <td><select name="catids[]" id="catids" multiple="multiple" style="height:200px;" title="按住“Ctrl”或“Shift”键可以多选，按住“Ctrl”可取消选择">
              {$category}
            </select></td>
        </tr>
        <tr>
          <th>更新周期:</th>
          <td><input type="text" name="time" value="{$config.time}" size="20" class="input"></td>
        </tr>
        <tr>
          <th>Email:</th>
          <td><input type="text" name="email" value="{$config.email}" size="20" class="input"></td>
        </tr>
        <tr>
          <th>生成数量:</th>
          <td><input type="text" name="baidunum" value="{$config.baidunum}" size="5" class="input"></td>
        </tr>
      </table>
      <div class="btn_wrap">
        <div class="btn_wrap_pd">
          <button class="btn btn_submit mr10 J_ajax_submit_btn1" type="submit">提交</button>
        </div>
      </div>
    </div>
  </form>
</div>
<script src="{$config_siteurl}statics/js/common.js?v"></script>
</body>
</html>